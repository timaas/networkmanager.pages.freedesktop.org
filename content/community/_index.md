+++
draft= false
title = "Community"
description = "How to get in touch with the NetworkManager community."
+++

### Repository and issue tracker

https://gitlab.freedesktop.org/NetworkManager/NetworkManager

### Mailing list

http://mail.gnome.org/mailman/listinfo/networkmanager-list

### IRC

#nm on freenode
