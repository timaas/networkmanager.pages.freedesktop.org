---
title: "Video tutorials"
date:
weight: 6
summary: ""
description: ""
bref: ""
toc: true
---

### nm-cloud-setup on Amazon EC2

{{< youtube  Kf2SosBySrY >}}

&nbsp;

### WireGuard configuration

{{< youtube  C6AkfVwMq44 >}}

&nbsp;