---
title: "NetworkManager for administrators"
date:
weight: 4
summary: "Information for administrators"
toc: true
---

### Architecture

[NetworkManager](https://developer.gnome.org/NetworkManager/stable/NetworkManager.html)
is designed to be fully automatic by default. It manages the primary
network connection and other network interfaces, like Ethernet, Wi-Fi,
and Mobile Broadband devices. To use NetworkManager, its service must
be started. Starting up NetworkManager depends on the distribution you
are running, but NetworkManager ships with systemd service files to do
this for most distributions. NetworkManager will then automatically
start other services as it requires them (wpa_supplicant for WPA and
802.1x connections, pppd for mobile broadband).

#### Security

NetworkManager supports most network security methods and protocols,
WPA/WPA2/WPA3 (Personal and Enterprise), wired 802.1x, MACsec and
VPNs. NetworkManager stores network secrets (encryption keys, login
information) using secure storage, either in the user's keyring (for
user-specific connections) or protected by normal system administrator
permissions (like root) for system-wide connections. Various network
operations can be locked down with
[polkit](https://www.freedesktop.org/software/polkit/docs/latest) for
even finer grained control over network connections.

#### VPN

NetworkManager has pluggable support for VPN software, including Cisco
compatible VPNs (using vpnc), openvpn, and Point-to-Point Tunneling
Protocol (PPTP). Support for other vpn clients is welcomed. Simply
install the NetworkManager VPN plugin your site uses, and pre-load the
user's machines with the VPN's settings. The first time they connect,
the user will be asked for their passwords.

See the [VPN](/docs/vpn) page for more details.

### Configuration files

[NetworkManager.conf](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/master/contrib/fedora/rpm/NetworkManager.conf)
is the configuration file for NetworkManager. It is used to set up
various aspects of NetworkManager's behavior.

If a default `NetworkManager.conf` is provided by your distribution's
packages, you should not modify it, since your changes may get
overwritten by package updates. Instead, you can add additional files
with `.conf` extension to the `/etc/NetworkManager/conf.d`
directory. These will be read in order, with later files overriding
earlier ones.

Packages might install further configuration snippets to
`/usr/lib/NetworkManager/conf.d`. This directory is parsed first, even
before `NetworkManager.conf`. Scripts can also put per-boot
configuration into `/run/NetworkManager/conf.d`. This directory is
parsed second, also before `NetworkManager.conf`.

### Server-like behavior

By default NetworkManager automatically creates a new in-memory
connection for every Ethernet device that doesn't have another
candidate connection on disk. These new connections have name "Wired
connection 1", "Wired connection 2" and so on; they have DHCPv4 and
IPv6 autoconfiguration enabled.

This behavior is usually not desirable on servers, where all
interfaces should be configured explicitly. To disable the creation of
such automatic connections, add `no-auto-default=*` to the `[main]`
configuration section.

Also, NetworkManager requires carrier on an interface before a
connection can be activated on it. If there are services that need to
bind to the interface address at boot, they might fail if the
interface has no carrier. The solution is to disable carrier detection
with configuration option `ignore-carrier=*` in the `[main]` section.

Note that on Fedora and RHEL there is a `NetworkManager-config-server`
package that install a configuration snippet with the two options
described above.

### Unmanaging devices

By default NetworkManager manages all devices found on the system. If
you plan to configure an interface manually or through some other
tool, you should tell NetworkManager to not manage it.

To do this temporarily until the next reboot use command
```
nmcli device set enp1s0 managed no
```

If you want the choice to persist after a reboot, add the following
snippet to configuration:

```
[device-enp1s0-unmanage]
match-device=interface-name:enp1s0
managed=0
```
Then, remember to reload configuration with `systemctl reload
NetworkManager`.
