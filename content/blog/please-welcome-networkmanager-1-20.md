+++
date = "2019-08-06T12:00:00+01:00"
title = "Please welcome: NetworkManager 1.20 [link]"
tags = ["release"]
draft = false
author = "Lubomir Rintel"
description = "A quick overview of what’s new"
weight = 10
+++

Another three months have passed since NetworkManager’s 1.18, and 1.20 is now available. What follows is a quick overview of what’s new.

Read more on [Lubomir Rintel's web log](https://blogs.gnome.org/lkundrak/2019/08/06/networkmanager-1-20/).
