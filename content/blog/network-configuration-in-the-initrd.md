+++
date = "2020-11-24T12:00:00+01:00"
title = "Looking forward to Linux network configuration in the initial ramdisk (initrd) [link]"
tags = ["initrd"]
draft = false
author = "Beniamino Galvani"
description = "One of the tasks that the initrd might be responsible for is network configuration"
weight = 10
+++

One of the tasks that the initrd might be responsible for is network
configuration. This article explains the cases in which network
configuration early in the boot process is necessary, how it's
implemented, and the improvements that Red Hat Enterprise Linux 8.3
brings.

Read more on the [Enable Sysadmin Blog](https://www.redhat.com/sysadmin/network-confi-initrd).
